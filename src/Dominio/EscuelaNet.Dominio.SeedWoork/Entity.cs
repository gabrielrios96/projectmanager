﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EscuelaNet.Dominio.SeedWoork
{
    public class Entity
    {
        private int _id;
        public virtual int ID
        {
            get
            {
                return _id;
            }
            protected set {
                _id = value;
            }
        }
        public bool IsTransient()
        {
            return this.ID != default(int);
        }
    }
}
